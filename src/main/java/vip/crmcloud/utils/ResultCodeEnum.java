package vip.crmcloud.utils;

/**
 * 统一返回结果状态信息类
 *
 */
public enum ResultCodeEnum {

    SUCCESS(200,"success"),
    USERNAME_ERROR(501,"账号不存在"),
    PASSWORD_ERROR(503,"密码错误"),
    NOTLOGIN(504,"notLogin"),
    USERNAME_USED(505,"账号已使用");

    private Integer code;
    private String message;
    private ResultCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
    public Integer getCode() {
        return code;
    }
    public String getMessage() {
        return message;
    }
}
