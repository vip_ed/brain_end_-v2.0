package vip.crmcloud.controller;

import com.google.protobuf.Any;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import vip.crmcloud.mapper.CategoryMapper;
import vip.crmcloud.pojo.Category;
import vip.crmcloud.service.CategoryService;
import vip.crmcloud.utils.Result;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class CategoryController {

    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private CategoryService categoryService;
   // 获取文章分类
    @GetMapping("getCategorys")
    public Result getCategorys(){
        List<Map<String, Object>> data = categoryService.findCategoryAll();
//        System.out.println(data);
//        if(!data.isEmpty()){
            return Result.ok(data);
//        } else {
//            return Result.fail(400,"暂无数据");
//        }
    }

}
