package vip.crmcloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vip.crmcloud.mapper.RoleMapper;
import vip.crmcloud.pojo.Role;
import vip.crmcloud.utils.Result;

import java.util.List;

@RestController
@CrossOrigin
public class RoleController {
    @Autowired
    private RoleMapper roleMapper;

    @GetMapping("role/list")
    public Result getRoleList(){
        List<Role> roleList = roleMapper.selectList(null);
        return Result.ok(roleList);
    }
}
