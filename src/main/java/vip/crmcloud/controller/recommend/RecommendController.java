package vip.crmcloud.controller.recommend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.crmcloud.pojo.Article;
import vip.crmcloud.service.ArticleService;
import vip.crmcloud.utils.Result;
@CrossOrigin
@RestController
public class RecommendController {
    @Autowired
    private ArticleService articleService;
    @PostMapping("recommendList")
    public Result recommendList(@RequestBody Article article){
        Result result = articleService.recommendList(article.getUid());
        // 前端只传uid，后端进行逻辑判断，联表查询
        if(article.getUid().isEmpty()){
            return Result.fail(400,"请传入正确参数");
        }
        return result;
    }
}
