package vip.crmcloud.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import vip.crmcloud.mapper.ArticleMapper;
import vip.crmcloud.params.QueryParams;
import vip.crmcloud.pojo.Article;
import vip.crmcloud.service.ArticleService;
import vip.crmcloud.utils.Result;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


// 获取文章列表
@CrossOrigin // 跨域
@RestController // 返回的不再是html视图，而是直接将结果返回
public class ArticleController {
    @Autowired
    private ArticleMapper articleMapper;

    @Autowired
    private ArticleService articleService;

    // 查询文章，有aid按aid分类查，没有就全部查，有关键字就按关键字查，没有就全部查
    @PostMapping("layout/articleList")
    public Result findArticles(
            @RequestBody QueryParams queryParams
    ) {
        LambdaQueryWrapper<Article> queryWrapper = new LambdaQueryWrapper<>();
        // 判断是否有aid
        if (queryParams.getAid() != null && queryParams.getAid() != -1) {
            queryWrapper.eq(Article::getAid, queryParams.getAid());
        }
        // 判断是否有uid
        if (queryParams.getUid() != null && !queryParams.getUid().isEmpty()) {
            queryWrapper.eq(Article::getUid, queryParams.getUid());
        }
        // 判断是否有scid
        if (queryParams.getScid() != null && queryParams.getScid() != -1 && !queryParams.getScid().equals("undefined")) {
            queryWrapper.eq(Article::getScid, queryParams.getScid());
        }
        // 判断是否有keyword
        if (queryParams.getKeyword() != null && !queryParams.getKeyword().isEmpty()) {
            queryWrapper.like(Article::getArticleContent, queryParams.getKeyword());
        }

        List<Article> articles = articleMapper.selectList(queryWrapper);
        if (!articles.isEmpty()) {
            return Result.ok(articles);

        } else {
            // 操作失败
            return Result.fail(400, "暂无数据");
        }
    }
    // 编辑文章
    @PostMapping("editArticle")
    public Result editArticle(@RequestBody Article article) {
        Integer count = articleService.editArticle(article);
        Map<String, Integer> data = new HashMap<>();
        if(count > 0){
            data.put("count",count);
            return Result.ok(data);
        }
        return Result.fail(400,"编辑失败");
    }
    // 删除文章
    @PostMapping("delArticle")
    public Result delArticle(@RequestBody Article article) {
        Integer count = articleService.delArticle(article);
        Map<String, Integer> data = new HashMap<>();
        if(count > 0){
            data.put("count",count);
            return Result.ok(data);
        }
        return Result.fail(400,"删除失败");
    }
    // 发布文章
    @PostMapping("addArticle")
    public Result addArticle(@RequestBody Article article) {
        Result result = articleService.addArticle(article);
        return result;
    }

    // 查询文章详情，要求把views+1
    @PostMapping("article/detail")
    public Result showArticleDetail(@RequestBody Article article) {
        Result result = articleService.showArticleDetail(article.getAid());
        return result;
    }

    // 查询文章点赞量
    @PostMapping("article/likes")
    public Result articleLikes(@RequestBody Map<String, Map<String, String>> map) {
        if (map.containsKey("data")) {
            Result result = articleService.articleLikes(map.get("data"));
            return result;
        }
        return Result.fail(400, "请注意参数格式");
    }

    // 查询文章收藏量
    @PostMapping("article/stars")
    public Result articleStars(@RequestBody Map<String, Map<String, String>> map) {
        if (map.containsKey("data")) {
            Result result = articleService.articleStars(map.get("data"));
            return result;
        }
        return Result.fail(400, "请注意参数格式");
    }

    // 查询文章评论量
    @PostMapping("article/reviews")
    public Result articleReviews(@RequestBody Map<String, Map<String, String>> map) {
        if (map.containsKey("data")) {
            Result result = articleService.articleReviews(map.get("data"));
            return result;
        }
        return Result.fail(400, "请注意参数格式");
    }

    // 查询文章评论列表
    @PostMapping("article/reviewsList")
    public Result articleReviewsList(@RequestBody Map<String, Map<String, String>> map) {
        if (map.containsKey("data")) {
            Result result = articleService.articleReviewsList(map.get("data"));
            return result;
        }
        return Result.fail(400, "请注意参数格式");
    }

}
