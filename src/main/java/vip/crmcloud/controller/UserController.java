package vip.crmcloud.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vip.crmcloud.pojo.User;
import vip.crmcloud.service.UserService;
import vip.crmcloud.utils.Result;
import vip.crmcloud.pojo.temp.Token;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class UserController {

    @Autowired
    private UserService userService;

    // 根据uid拿用户信息
    @PostMapping("userInfoByUid")
    public Result getUserInfoByUid(@RequestBody User user) {
        if (user.getUid() != null) {
            User userInfo = userService.getUserInfoByIdService(user.getUid());
            if (userInfo != null) {
                return Result.ok(userInfo);
            }
        }
        return Result.fail(400, "未查到用户信息");
    }

    // 登录接口
    @PostMapping("login")
    public Result login(@RequestBody User user) {
        Result result = userService.login(user);
        return result;
    }

    // 根据token拿用户信息，其实就是反解析id
    @GetMapping("getUserInfo")
    public Result getUserInfo(@RequestParam String token) {
        Result result = userService.getUserInfo(token);
        return result;
    }

    /**
     * 注册逻辑，直接写一个接口，先查输入的username是否已使用，通过count判断
     * count > 0 返回已使用
     * 否则就使用这个username和password注册账号，但是传的是个user对象，只有昵称账号密码
     * 请求体传参
     */
    @PostMapping("register")
    public Result register(@RequestBody User user) {
        Result result = userService.register(user);
        return result;
    }

    // 验证token有效性
    @PostMapping("token")
    public Result isToken(@RequestBody Token token) {
        Result result = userService.isToken(token);
        return result;
    }

    // 用户列表
    @GetMapping("userList")
    public Result articleLikes() {
        List<User> userList = userService.findUserList();
        if (userList.size() > 0) {
            return Result.ok(userList);
        } else {
            return Result.fail(400, "暂无数据");
        }
    }

    // 新增用户
    @PostMapping("add_user")
    public Result addUser(@RequestBody User user) {
        Integer count = userService.addUser(user);
        Map data = new HashMap();
        if (count > 0) {
            data.put("count", count);
            return Result.ok(data);
        }
        return Result.fail(400, "新增失败");
    }


    // 编辑用户
    @PostMapping("edit_user")
    public Result editUser(@RequestBody User user) {
        Integer count = userService.editUser(user);
        Map<String, Integer> data = new HashMap<>();
        if (count > 0) {
            data.put("count", count);
            return Result.ok(data);
        }
        return Result.fail(400, "编辑失败");
    }
    // 查找用户
    @PostMapping("user")
    public Result User(@RequestBody User user) {
        User info = userService.User(user);
        Map<String, User> data = new HashMap<>();
        if (user != null) {
            data.put("user", info);
            return Result.ok(data);
        }
        return Result.fail(400, "未找到");
    }

    // 删除用户
    @PostMapping("delete_user")
    public Result deleteUser(@RequestBody User user) {
        Integer count = userService.deleteUserById(user.getUid());
        Map<String, Integer> data = new HashMap<>();
        if (count > 0) {
            data.put("count", count);
            return Result.ok(data);
        }
        return Result.fail(400, "删除失败");
    }
}
