package vip.crmcloud.controller.userArticle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.crmcloud.pojo.userArticle.UserLike;
import vip.crmcloud.service.userArticle.UserLikeService;
import vip.crmcloud.utils.Result;

@CrossOrigin
@RestController
public class UserLikeController {
    @Autowired
    private UserLikeService userLikeService;

    // TODO现在Scid没插入，有bug
    @PostMapping("addLike")
    public Result addLike(@RequestBody UserLike userLike) {
        // 要aid和uid都不为空才能操作
        Boolean res = !userLike.getUid().isEmpty() && !userLike.getAid().isEmpty();
        if (res) {
            Boolean result = userLikeService.addLike(userLike);
            if (result) {
                return Result.ok("成功");
            } else {
                return Result.fail(400, "失败");
            }
        } else {
            return Result.fail(400, "参数错误");
        }
    }
}
