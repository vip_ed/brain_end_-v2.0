package vip.crmcloud.controller.userArticle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.crmcloud.pojo.userArticle.UserLike;
import vip.crmcloud.pojo.userArticle.UserStar;
import vip.crmcloud.service.userArticle.UserLikeService;
import vip.crmcloud.service.userArticle.UserStarService;
import vip.crmcloud.utils.Result;

@CrossOrigin
@RestController
public class UserStarController {
    @Autowired
    private UserStarService userStarService;

    @PostMapping("addStar")
    public Result addLike(@RequestBody UserStar userStar) {
        // 要aid和uid都不为空才能操作
        Boolean res = !userStar.getUid().isEmpty() && !userStar.getAid().isEmpty();
        if (res) {
            Boolean result = userStarService.addStar(userStar);
            if (result) {
                return Result.ok("成功");
            } else {
                return Result.fail(400, "失败");
            }
        } else {
            return Result.fail(400, "参数错误");
        }
    }
}
