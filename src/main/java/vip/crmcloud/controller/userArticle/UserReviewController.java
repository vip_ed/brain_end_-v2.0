package vip.crmcloud.controller.userArticle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.crmcloud.service.userArticle.UserReviewService;
import vip.crmcloud.utils.Result;

import java.util.Map;

@CrossOrigin
@RestController
public class UserReviewController {
    @Autowired
    private UserReviewService userReviewService;
    // 新增评论
    @PostMapping("addReview")
    public Result articleLikes(@RequestBody Map<String, Map<String, String>> map) {
        // 一定要有aid，uid和review_content,前端控制一定不为空
        if (map.containsKey("data")) {
            Boolean result = userReviewService.addReview(map.get("data"));
            if(result){
                return Result.ok("留言成功");
            }
        }
        return Result.fail(400,"留言失败");
    }
}
