package vip.crmcloud.service;

import vip.crmcloud.pojo.Subcategory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author jevons
* @description 针对表【brain_subcategory】的数据库操作Service
* @createDate 2024-03-27 07:58:12
*/
public interface SubcategoryService extends IService<Subcategory> {

}
