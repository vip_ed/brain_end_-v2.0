package vip.crmcloud.service;

import vip.crmcloud.pojo.User;
import com.baomidou.mybatisplus.extension.service.IService;
import vip.crmcloud.pojo.temp.Token;
import vip.crmcloud.utils.Result;

import java.util.List;
import java.util.Map;

/**
* @author 16597
* @description 针对表【brain_user】的数据库操作Service
* @createDate 2023-11-12 10:08:37
*/
public interface UserService extends IService<User> {

    // 登录接口
    Result login(User user);

    Result getUserInfo(String token);

    Result register(User user);

    User getUserInfoByIdService(String uid);

    // 验证token是否有效
    Result isToken(Token token);

    // 查找用户列表
    List<User> findUserList();
    // 新增用户
    Integer addUser(User user);
    // 编辑用户
    Integer editUser(User user);
    // 查找用户
    User User(User user);
    // 删除用户
    Integer deleteUserById(String uid);

}
