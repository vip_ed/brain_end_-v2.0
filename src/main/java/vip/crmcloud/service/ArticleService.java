package vip.crmcloud.service;

import vip.crmcloud.pojo.Article;
import com.baomidou.mybatisplus.extension.service.IService;
import vip.crmcloud.utils.Result;

import java.util.Map;

/**
* @author 16597
* @description 针对表【brain_article】的数据库操作Service
* @createDate 2023-11-12 10:08:37
*/
public interface ArticleService extends IService<Article> {
    /**
     * 根据id查详情
     * @param aid
     * @return
     */
    Result showArticleDetail(String aid);

    // 发布文章
    Result addArticle(Article article);
    // 编辑文章
    Integer editArticle(Article article);
    // 删除文章
    Integer delArticle(Article article);

    // 查询文章点赞量
    Result articleLikes(Map<String,String> data);
    // 查询文章收藏量
    Result articleStars(Map<String,String> data);
    // 查询文章评论量
    Result articleReviews(Map<String,String> data);
    // 查询文章评论列表
    Result articleReviewsList(Map<String,String> data);
    // 推荐文章列表
    Result recommendList(String uid);

}
