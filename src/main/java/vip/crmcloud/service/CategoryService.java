package vip.crmcloud.service;

import vip.crmcloud.pojo.Category;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
* @author 16597
* @description 针对表【brain_category】的数据库操作Service
* @createDate 2023-11-12 10:08:37
*/
public interface CategoryService extends IService<Category> {

    List<Map<String, Object>> findCategoryAll();
}
