package vip.crmcloud.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import vip.crmcloud.mapper.SubcategoryMapper;
import vip.crmcloud.pojo.Category;
import vip.crmcloud.pojo.Subcategory;
import vip.crmcloud.service.CategoryService;
import vip.crmcloud.mapper.CategoryMapper;
import org.springframework.stereotype.Service;
import vip.crmcloud.utils.Result;

import java.util.*;

/**
 * @author 16597
 * @description 针对表【brain_category】的数据库操作Service实现
 * @createDate 2023-11-12 10:08:37
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category>
        implements CategoryService {
    private List<Subcategory> subcategory;

    public List<Subcategory> getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(List<Subcategory> subcategory) {
        this.subcategory = subcategory;
    }

    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private SubcategoryMapper subcategoryMapper;

    // 实现数据查询及封装，层级明确
    @Override
    public List<Map<String, Object>> findCategoryAll() {
        List<Map<String, Object>> result = new ArrayList<>();

        // 查询所有的一级菜单
        List<Category> categoryList = categoryMapper.selectList(null);

        for (Category category : categoryList) {
            Map<String, Object> resultMap = new HashMap<>();
            // 设置一级菜单的 id 和 label
            resultMap.put("id", category.getCid());
            resultMap.put("label", category.getCategoryName());
            // 查询当前一级菜单的子类别列表
            LambdaQueryWrapper<Subcategory> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(Subcategory::getCid, category.getCid());
            List<Subcategory> subcategoryList = subcategoryMapper.selectList(queryWrapper);
            // 将子类别列表处理成 id 和 label 形式
            List<Map<String, Object>> formattedSubcategoryList = new ArrayList<>();
            for (Subcategory subcategory : subcategoryList) {
                Map<String, Object> subcategoryMap = new HashMap<>();
                subcategoryMap.put("id", subcategory.getScid());
                subcategoryMap.put("label", subcategory.getSubcategoryName());
                formattedSubcategoryList.add(subcategoryMap);
            }
            // 将子类别列表放入一级菜单中
            resultMap.put("child", formattedSubcategoryList);
            // 将构建好的一级菜单放入结果列表中
            result.add(resultMap);
        }
        return result;
    }

}




