package vip.crmcloud.service.impl.userArticleimpl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vip.crmcloud.mapper.ArticleMapper;
import vip.crmcloud.mapper.userArticle.UserLikeMapper;
import vip.crmcloud.pojo.userArticle.UserLike;
import vip.crmcloud.service.userArticle.UserLikeService;

// 返回查到的数据
@Service
public class UserLikeServiceImpl extends ServiceImpl<UserLikeMapper, UserLike>
        implements UserLikeService {
    @Autowired
    private UserLikeMapper userLikeMapper;
    @Autowired
    private ArticleMapper articleMapper;
    // 用户点赞
    @Override
    public Boolean addLike(UserLike userLike) {
        // 要根据uid和aid判断是哪条数据
        LambdaQueryWrapper<UserLike> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserLike::getUid, userLike.getUid());
        queryWrapper.eq(UserLike::getAid, userLike.getAid());

        // 根据查询条件查询数据库中的记录
        UserLike existingUserLike = userLikeMapper.selectOne(queryWrapper);
        if (existingUserLike != null) {
            // 有记录说明这个文章这个用户已经点过赞，不能让他再点赞了
            return false;
        } else {
            // 如果未找到符合条件的记录，则插入一条新的记录,并且在article内like加一
            userLike.setLikeCount(1L);
            int rowsAffected = userLikeMapper.insert(userLike);
            articleMapper.updateLikes(userLike.getAid());
            return rowsAffected > 0;
        }
    }
}





