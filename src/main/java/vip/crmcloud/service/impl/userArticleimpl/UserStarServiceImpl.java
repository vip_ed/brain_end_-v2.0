package vip.crmcloud.service.impl.userArticleimpl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vip.crmcloud.mapper.ArticleMapper;
import vip.crmcloud.mapper.userArticle.UserLikeMapper;
import vip.crmcloud.mapper.userArticle.UserStarMapper;
import vip.crmcloud.pojo.userArticle.UserLike;
import vip.crmcloud.pojo.userArticle.UserStar;
import vip.crmcloud.service.userArticle.UserLikeService;
import vip.crmcloud.service.userArticle.UserStarService;

// 返回查到的数据
@Service
public class UserStarServiceImpl extends ServiceImpl<UserStarMapper, UserStar>
        implements UserStarService {
    @Autowired
    private UserStarMapper userStarMapper;
    @Autowired
    private ArticleMapper articleMapper;
    // 用户点赞
    @Override
    public Boolean addStar(UserStar userStar) {
        // 要根据uid和aid判断是哪条数据
        LambdaQueryWrapper<UserStar> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserStar::getUid, userStar.getUid());
        queryWrapper.eq(UserStar::getAid, userStar.getAid());

        // 根据查询条件查询数据库中的记录
        UserStar existingUserLike = userStarMapper.selectOne(queryWrapper);
        if (existingUserLike != null) {
            // 有记录说明这个文章这个用户已经点过赞，不能让他再点赞了
            return false;
        } else {
            // 如果未找到符合条件的记录，则插入一条新的记录
            userStar.setStarCount(1L);
            int rowsAffected = userStarMapper.insert(userStar);
            articleMapper.updateStars(userStar.getAid());
            return rowsAffected > 0;
        }
    }
}





