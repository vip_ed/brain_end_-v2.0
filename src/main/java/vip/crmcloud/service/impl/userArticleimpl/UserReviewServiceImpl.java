package vip.crmcloud.service.impl.userArticleimpl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vip.crmcloud.mapper.userArticle.UserReviewMapper;
import vip.crmcloud.pojo.userArticle.UserReview;
import vip.crmcloud.service.userArticle.UserReviewService;

import java.util.Map;

// 返回查到的数据
@Service
public class UserReviewServiceImpl extends ServiceImpl<UserReviewMapper, UserReview>
        implements UserReviewService {
    @Autowired
    private UserReviewMapper userReviewMapper;

    // 用户点赞
    @Override
    public Boolean addReview(Map<String, String> data) {
        UserReview userReview = new UserReview();
        // 全部都是新增评论
        if (!data.get("aid").isEmpty() && !data.get("uid").isEmpty()) {
            userReview.setAid(data.get("aid"));
            userReview.setUid(data.get("uid"));
            userReview.setReviewContent(data.get("review_content"));
            Integer count = userReviewMapper.insert(userReview);
            if (count != 0) {
                return true;
            }
        }
        return false;
    }
}





