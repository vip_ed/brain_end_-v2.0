package vip.crmcloud.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.crmcloud.pojo.Study;
import vip.crmcloud.service.StudyService;
import vip.crmcloud.mapper.StudyMapper;
import org.springframework.stereotype.Service;

/**
* @author 16597
* @description 针对表【brain_study】的数据库操作Service实现
* @createDate 2023-11-12 10:08:37
*/
@Service
public class StudyServiceImpl extends ServiceImpl<StudyMapper, Study>
    implements StudyService{

}




