package vip.crmcloud.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.crmcloud.pojo.Subcategory;
import vip.crmcloud.service.SubcategoryService;
import vip.crmcloud.mapper.SubcategoryMapper;
import org.springframework.stereotype.Service;

/**
* @author jevons
* @description 针对表【brain_subcategory】的数据库操作Service实现
* @createDate 2024-03-27 07:58:12
*/
@Service
public class SubcategoryServiceImpl extends ServiceImpl<SubcategoryMapper, Subcategory>
    implements SubcategoryService{

}




