package vip.crmcloud.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.crmcloud.pojo.Role;
import vip.crmcloud.service.RoleService;
import vip.crmcloud.mapper.RoleMapper;
import org.springframework.stereotype.Service;

/**
* @author 16597
* @description 针对表【brain_role】的数据库操作Service实现
* @createDate 2023-11-12 10:08:37
*/
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role>
    implements RoleService{

}




