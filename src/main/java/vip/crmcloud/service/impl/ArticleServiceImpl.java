package vip.crmcloud.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import vip.crmcloud.mapper.userArticle.UserLikeMapper;
import vip.crmcloud.mapper.userArticle.UserReviewMapper;
import vip.crmcloud.mapper.userArticle.UserStarMapper;
import vip.crmcloud.pojo.Article;
import vip.crmcloud.pojo.userArticle.UserLike;
import vip.crmcloud.pojo.article.ArticleDetail;
import vip.crmcloud.pojo.userArticle.UserReview;
import vip.crmcloud.pojo.userArticle.UserStar;
import vip.crmcloud.service.ArticleService;
import vip.crmcloud.mapper.ArticleMapper;
import org.springframework.stereotype.Service;
import vip.crmcloud.utils.Result;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author 16597
 * @description 针对表【brain_article】的数据库操作Service实现
 * @createDate 2023-11-12 10:08:37
 */
@Service
public class ArticleServiceImpl extends ServiceImpl<ArticleMapper, Article> implements ArticleService {
    @Autowired
    private ArticleMapper articleMapper;

    @Autowired
    private UserLikeMapper userLikeMapper;
    @Autowired
    private UserStarMapper userStarMapper;
    @Autowired
    private UserReviewMapper userReviewMapper;

    /**
     * 修改阅读量，因为有乐观锁，要先拿到版本号，所以要先查
     *
     * @param aid
     * @return
     */
    // TODO编辑文章和文章详情现在是一样的逻辑，不需要写两个接口，service层
    //  查询文章详情
    @Override
    public Result showArticleDetail(String aid) {
        ArticleDetail articleDeatil = articleMapper.queryDetailMap(aid);
        if (articleDeatil == null) {
            return Result.fail(400, "文章不存在");
        }
//        // TODO修改阅读量之前应该判断当前作者是否已经阅读过,到时候再想怎么实现
//        // 阅读量加一

        // 调用xml,sql更改数据库阅读量
        articleMapper.updateViews(aid);
        Map data = new HashMap();
        data.put("detail", articleDeatil);
        return Result.ok(data);
    }

    // 编辑文章
    @Override
    public Integer editArticle(Article article) {
        if (article.getAid().isEmpty()) {
            // 如果 userId 为空，说明用户 id 不合法，删除失败，返回 0
            return 0;
        }

        // 根据 userId 查询用户是否存在
        Article existingUser = articleMapper.selectByAid(article.getAid());
        if (existingUser == null) {
            // 如果根据 userId 没有查询到用户，说明用户不存在，删除失败，返回 0
            return 0;
        }
        try {
            // 执行编辑操作
            return articleMapper.updateByAid(article);
        } catch (Exception e) {
            // 可以根据具体的异常情况进行处理，这里简单起见，直接返回 0
            return 0;
        }
    }

    // 删除文章
    @Override
    public Integer delArticle(Article article) {
        if (article.getAid().isEmpty()) {
            // 如果 userId 为空，说明用户 id 不合法，删除失败，返回 0
            return 0;
        }

        // 根据 userId 查询用户是否存在
        Article existingUser = articleMapper.selectByAid(article.getAid());
        if (existingUser == null) {
            return 0;
        }
        try {
            // 执行编辑操作
            return articleMapper.deleteByAid(article);
        } catch (Exception e) {
            // 可以根据具体的异常情况进行处理，这里简单起见，直接返回 0
            log.error(e.getMessage(), e);
            return 0;
        }
    }

    // 发布文章  后端来获取事件，不用前端写
    @Override
    public Result addArticle(Article article) {
        Integer count = articleMapper.insert(article);
        if (count > 0) {
            return Result.ok("发布成功");
        } else {
            return Result.fail(400, "发布失败");
        }
    }

    // 文章点赞量
    @Override
    public Result articleLikes(Map<String, String> data) {
        if (!data.isEmpty() && data.get("aid") != null) {
            LambdaQueryWrapper<UserLike> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(UserLike::getAid, data.get("aid"));
            List<UserLike> userArticleList = userLikeMapper.selectList(queryWrapper);
            // 增强for统计点赞量
            Long count = 0L;
            for (UserLike userArticle : userArticleList) {
                count = count + userArticle.getLikeCount();
            }
            Map map = new HashMap<>();
            map.put("count", count);
            return Result.ok(map);
        }
        return Result.fail(400, "请传入正确参数");
    }

    // 文章收藏量
    @Override
    public Result articleStars(Map<String, String> data) {
        if (!data.isEmpty() && data.get("aid") != null) {
            LambdaQueryWrapper<UserStar> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(UserStar::getAid, data.get("aid"));
            List<UserStar> userArticleList = userStarMapper.selectList(queryWrapper);
            // 增强for统计点赞量
            Long count = 0L;
            for (UserStar userArticle : userArticleList) {
                count = count + userArticle.getStarCount();
            }
            Map map = new HashMap<>();
            map.put("count", count);
            return Result.ok(map);
        }
        return Result.fail(400, "请传入正确参数");
    }

    // 文章评论量
    @Override
    public Result articleReviews(Map<String, String> data) {
        if (!data.isEmpty() && data.get("aid") != null) {
            LambdaQueryWrapper<UserReview> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(UserReview::getAid, data.get("aid"));
            List<UserReview> userArticleList = userReviewMapper.selectList(queryWrapper);
            int count = 0;
            count = userArticleList.size();
            Map map = new HashMap<>();
            map.put("count", count);
            return Result.ok(map);
        }
        return Result.fail(400, "请传入正确参数");
    }

    // 文章评论列表
    @Override
    public Result articleReviewsList(Map<String, String> data) {
        if (!data.isEmpty() && data.get("aid") != null) {
            LambdaQueryWrapper<UserReview> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(UserReview::getAid, data.get("aid"));
            List<UserReview> userArticleList = userReviewMapper.selectList(queryWrapper);
            int count = 0;
            count = userArticleList.size();
            if (count > 0) {
                Map map = new HashMap<>();
                map.put("list", userArticleList);
                return Result.ok(map);
            } else {
                Map map = new HashMap<>();
                map.put("list", userArticleList);
                return Result.fail(400, "暂无数据");
            }
        }
        return Result.fail(400, "请传入正确参数");
    }

    // 推荐列表
    @Override
    public Result recommendList(String uid) {
        String scid = userStarMapper.selectByUid(uid);
        if (scid == null || scid.isEmpty()) {
            return Result.fail(400, "请收藏后查询");
        }
        return Result.ok(scid);
    }

}




