package vip.crmcloud.service.impl;

import com.alibaba.druid.util.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import vip.crmcloud.pojo.User;
import vip.crmcloud.pojo.temp.Token;
import vip.crmcloud.service.UserService;
import vip.crmcloud.mapper.UserMapper;
import org.springframework.stereotype.Service;
import vip.crmcloud.utils.JwtHelper;
import vip.crmcloud.utils.MD5Util;
import vip.crmcloud.utils.Result;
import vip.crmcloud.utils.ResultCodeEnum;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* @author 16597
* @description 针对表【brain_user】的数据库操作Service实现
* @createDate 2023-11-12 10:08:37
*/

// 返回查到的数据
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
    implements UserService{

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private JwtHelper jwtHelper;

    // 实现通过uid查用户
    @Override
    public User getUserInfoByIdService(String uid) {
        // 直接调用mapper返回结果就行
        User userInfo = userMapper.selectByUid(uid);
        return userInfo;
    }

    // 检查token是否有效，返回布尔值
    @Override
    public Result isToken(Token token) {
        // 获取 Token 字符串
        String tokenString = token.getToken();
        // 验证token有效性
        boolean expiration = jwtHelper.isExpiration(tokenString);
        // false为未过期
        if(expiration){
            return Result.fail(400,"令牌过期，请重新登录");
        }else {
            // 返回结果false是未过期
            return Result.ok(expiration);
        }
    }

    // 登录接口实现
    @Override
    public Result login(User user) {
        // 根据传来的user.name查询用户记录
        LambdaQueryWrapper<User> userLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userLambdaQueryWrapper.eq(User::getUserName,user.getUserName());
        User loginUser = userMapper.selectOne(userLambdaQueryWrapper);

        // 拿到通过用户名查到的用户后对比里面内容
        if(loginUser == null){
            // 如果没查到,则说明用户名为空
            return Result.build(null, ResultCodeEnum.USERNAME_ERROR);
        }

        // 校验密码
        // 输入的密码跟通过用户名查到的密码是否一致，要先解密
        if(!StringUtils.isEmpty(user.getUserPassword())
            && MD5Util.encrypt(user.getUserPassword()).equals(loginUser.getUserPassword())){
            // 成功就直接获取token返回
            // Long.valueOf强转
            String token = jwtHelper.createToken(Long.valueOf(loginUser.getUid()));
            Map data = new HashMap();
            data.put("token",token);
            return Result.ok(data);
        }
        // 失败走外层,因为用户名错误在上面已经走了，直接返回密码错误即可
        return Result.build(null,ResultCodeEnum.PASSWORD_ERROR);
    }


    // 根据token拿用户信息,拿的是除密码的所有东西
    @Override
    public Result getUserInfo(String token) {
        // 先检验是否失效
        boolean expiration = jwtHelper.isExpiration(token);
        if(expiration){
            // 失效未登录
            return Result.build(null,ResultCodeEnum.NOTLOGIN);
        }

        // 否则就是未失效，解析id，通过id拿用户信息
        Long userId = jwtHelper.getUserId(token);
        User user = userMapper.selectById(userId);
        // 返回数据前把密码去掉，不然就会泄露密码
        user.setUserPassword("");
        Map data =  new HashMap();
        data.put("loginUser",user);
        return Result.ok(data);
    }


    /**
     * 注册逻辑，直接写一个接口，先查输入的username是否已使用，通过count判断
     * count > 0 返回已使用
     * 否则就使用这个username和password注册账号，但是传的是个user对象，昵称账号密码
     * 请求体传参
     */
    @Override
    public Result register(User user) {
        // 目前只有昵称,账号,密码
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getUserName,user.getUserName());
        Long count = userMapper.selectCount(queryWrapper);
        if(count>0){
            return Result.build(null,ResultCodeEnum.USERNAME_USED);
        }

        // =0即可以使用，就加密密码
        user.setUserPassword(MD5Util.encrypt(user.getUserPassword()));
        userMapper.insert(user);
        return Result.ok(null);
    }
    // 查找用户列表
    @Override
    public List<User> findUserList() {
        List<User> userList = userMapper.selectList(null);
        return userList;
    }

    // 新增用户
    @Override
    public Integer addUser(User user) {
        try {
            return userMapper.insert(user);
        } catch (org.springframework.dao.DuplicateKeyException e) {
            // 如果捕获到唯一约束异常，则返回0或者其他相应的处理
            return 0;
        }
    }
    // 编辑用户
    @Override
    public Integer editUser(User user) {
        if (user.getUid() == null) {
            // 如果 uid 为空，说明用户信息不完整，编辑失败，返回 0
            return 0;
        }
        try {
            return userMapper.updateByUid(user);
        } catch (org.springframework.dao.DuplicateKeyException e) {
            // 如果捕获到唯一约束异常，则返回0或者其他相应的处理
            return 0;
        }
    }
    // 查找用户
    @Override
    public User User(User user) {
        if (user.getUid() == null) {
            // 如果 uid 为空，说明用户信息不完整，编辑失败，返回 0
            return null;
        }
        try {
            return userMapper.selectByUid(user.getUid());
        } catch (org.springframework.dao.DuplicateKeyException e) {
            // 如果捕获到唯一约束异常，则返回0或者其他相应的处理
            return null;
        }
    }

    // 删除用户
    @Override
    public Integer deleteUserById(String uid) {
        if (uid == null) {
            // 如果 userId 为空，说明用户 id 不合法，删除失败，返回 0
            return 0;
        }

        // 根据 userId 查询用户是否存在
        User existingUser = userMapper.selectByUid(uid);
        if (existingUser == null) {
            // 如果根据 userId 没有查询到用户，说明用户不存在，删除失败，返回 0
            return 0;
        }
        try {
            // 执行删除操作
            return userMapper.deleteByUid(uid);
        } catch (Exception e) {
            // 可以根据具体的异常情况进行处理，这里简单起见，直接返回 0
            return 0;
        }
    }
}




