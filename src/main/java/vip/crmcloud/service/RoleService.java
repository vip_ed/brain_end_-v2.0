package vip.crmcloud.service;

import vip.crmcloud.pojo.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 16597
* @description 针对表【brain_role】的数据库操作Service
* @createDate 2023-11-12 10:08:37
*/
public interface RoleService extends IService<Role> {

}
