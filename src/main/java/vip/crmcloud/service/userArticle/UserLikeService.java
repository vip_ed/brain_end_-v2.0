package vip.crmcloud.service.userArticle;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.crmcloud.pojo.userArticle.UserLike;

/**
* @author 16597
* @description 针对表【brain_article】的数据库操作Service
* @createDate 2023-11-12 10:08:37
*/
public interface UserLikeService extends IService<UserLike> {
    // 点赞
    Boolean addLike(UserLike userLike);
}
