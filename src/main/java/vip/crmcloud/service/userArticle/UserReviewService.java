package vip.crmcloud.service.userArticle;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.crmcloud.pojo.userArticle.UserReview;
import vip.crmcloud.pojo.userArticle.UserStar;

import java.util.Map;

/**
* @author 16597
* @description 针对表【brain_article】的数据库操作Service
* @createDate 2023-11-12 10:08:37
*/
public interface UserReviewService extends IService<UserReview> {
    // 点赞
    Boolean addReview(Map<String,String> data);

    /**
    * @author jevons
    * @description 针对表【user_star】的数据库操作Service
    * @createDate 2024-05-14 20:07:22
    */
    interface UserStarService extends IService<UserStar> {

    }
}
