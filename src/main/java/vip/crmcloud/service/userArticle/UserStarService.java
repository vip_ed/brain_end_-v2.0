package vip.crmcloud.service.userArticle;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.crmcloud.pojo.userArticle.UserStar;

public interface UserStarService extends IService<UserStar> {
    // 收藏
    Boolean addStar(UserStar userStar);
}
