package vip.crmcloud.params;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QueryParams {
    private Long aid;
    private Long cid;
    private Long scid;
    private String uid;
    private String keyword;
}
