package vip.crmcloud.mapper;

import org.apache.ibatis.annotations.Mapper;
import vip.crmcloud.pojo.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 16597
* @description 针对表【brain_category】的数据库操作Mapper
* @createDate 2023-11-12 10:08:37
* @Entity vip.crmcloud.pojo.Category
*/
@Mapper
public interface CategoryMapper extends BaseMapper<Category> {

}




