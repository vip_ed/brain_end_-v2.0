package vip.crmcloud.mapper;

import vip.crmcloud.pojo.Study;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 16597
* @description 针对表【brain_study】的数据库操作Mapper
* @createDate 2023-11-12 10:08:37
* @Entity vip.crmcloud.pojo.Study
*/
public interface StudyMapper extends BaseMapper<Study> {

}




