package vip.crmcloud.mapper;

import org.apache.ibatis.annotations.Mapper;
import vip.crmcloud.pojo.Subcategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author jevons
* @description 针对表【brain_subcategory】的数据库操作Mapper
* @createDate 2024-03-27 07:58:12
* @Entity vip.crmcloud.pojo.Subcategory
*/
@Mapper
public interface SubcategoryMapper extends BaseMapper<Subcategory> {

}




