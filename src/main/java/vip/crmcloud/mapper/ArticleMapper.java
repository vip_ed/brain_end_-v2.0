package vip.crmcloud.mapper;

import org.apache.ibatis.annotations.Mapper;
import vip.crmcloud.pojo.Article;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vip.crmcloud.pojo.User;
import vip.crmcloud.pojo.article.ArticleDetail;

import java.util.Map;

/**
* @author 16597
* @description 针对表【brain_article】的数据库操作Mapper
* @createDate 2023-11-12 10:08:37
* @Entity vip.crmcloud.pojo.Article
*/
@Mapper
public interface ArticleMapper extends BaseMapper<Article> {
    // 查详情的语句
    ArticleDetail queryDetailMap(String aid);
    // 更新浏览量---在xml中写个updata语句就行
    void updateViews(String aid);
    // 点赞加一
    void updateLikes(String aid);
    // 收藏加一
    void updateStars(String aid);
    // 通过aid查文章
    Article selectByAid(String aid);
    // 编辑文章
    Integer updateByAid(Article article);
    // 删除文章
    Integer deleteByAid(Article article);
}




