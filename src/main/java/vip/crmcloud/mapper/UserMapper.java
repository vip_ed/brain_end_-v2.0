package vip.crmcloud.mapper;

import org.apache.ibatis.annotations.Mapper;
import vip.crmcloud.pojo.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vip.crmcloud.pojo.article.ArticleDetail;

/**
* @author 16597
* @description 针对表【brain_user】的数据库操作Mapper
* @createDate 2023-11-12 10:08:37
* @Entity vip.crmcloud.pojo.User
*/
@Mapper
public interface UserMapper extends BaseMapper<User> {
    // 通过uid查用户
    User selectByUid(String uid);
    // 编辑用户
    Integer updateByUid(User user);
    // uid删除用户
    Integer deleteByUid(String uid);
}




