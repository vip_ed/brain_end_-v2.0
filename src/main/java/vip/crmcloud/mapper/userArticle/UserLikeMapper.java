package vip.crmcloud.mapper.userArticle;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import vip.crmcloud.pojo.userArticle.UserLike;

@Mapper
public interface UserLikeMapper extends BaseMapper<UserLike> {

}




