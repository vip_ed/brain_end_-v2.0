package vip.crmcloud.mapper.userArticle;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import vip.crmcloud.pojo.userArticle.UserLike;
import vip.crmcloud.pojo.userArticle.UserStar;

@Mapper
public interface UserStarMapper extends BaseMapper<UserStar> {
 String selectByUid(String uid);
}




