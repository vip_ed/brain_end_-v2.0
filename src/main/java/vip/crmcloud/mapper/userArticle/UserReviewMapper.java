package vip.crmcloud.mapper.userArticle;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import vip.crmcloud.pojo.userArticle.UserReview;

@Mapper
public interface UserReviewMapper extends BaseMapper<UserReview> {

}
