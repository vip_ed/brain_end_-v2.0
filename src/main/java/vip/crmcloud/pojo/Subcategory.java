package vip.crmcloud.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * @TableName brain_subcategory
 */
@TableName(value ="brain_subcategory")
@Data
public class Subcategory implements Serializable {
    private String scid;

    private String subcategoryName;

    private String cid;

    private Integer version;

    private Integer isDeleted;

    private static final long serialVersionUID = 1L;
}