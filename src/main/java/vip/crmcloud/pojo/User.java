package vip.crmcloud.pojo;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * @TableName brain_user
 */
@TableName(value = "brain_user")
@Data
public class User implements Serializable {
    @TableId
    private String uid;

    private String rid;

    private String userName;

    private String userPassword;

    private String userNick;

    private String userAvatar;

    private Date addTime = new Date();

    @Version
    private Integer version;

    private Integer isDeleted;

    private static final long serialVersionUID = 1L;
}
