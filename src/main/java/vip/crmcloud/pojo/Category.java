package vip.crmcloud.pojo;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * @TableName brain_category
 */
@TableName(value ="brain_category")
@Data
public class Category implements Serializable {
    @TableId
    private String cid;

    private String categoryName;

    @Version
    private Integer version;

    private Integer isDeleted;

    private static final long serialVersionUID = 1L;
}
