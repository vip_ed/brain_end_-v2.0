package vip.crmcloud.pojo.temp;

import lombok.Data;

@Data
public class Token {
    private String token;
}
