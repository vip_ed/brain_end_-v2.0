package vip.crmcloud.pojo.userArticle;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import lombok.Data;

import java.util.Date;

@TableName("user_star")
@Data
public class UserStar {
    @TableId
    private String usid;

    private String uid;

    private String aid;

    private String scid;

    private Date starTime = new Date();

    private Long starCount;

    @Version
    private Integer version;

    private Integer isDeleted;

    public UserStar() {
        this.starCount = 0L; // 设置默认值为0
    }
}
