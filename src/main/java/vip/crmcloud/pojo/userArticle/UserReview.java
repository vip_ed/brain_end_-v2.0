package vip.crmcloud.pojo.userArticle;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@TableName("user_review")
@Data
public class UserReview {
    @TableId
    private String urid;

    private String uid;

    private String aid;

    private String scid;

    private Date reviewTime = new Date();

    private String reviewContent;

    @Version
    private Integer version;

    private Integer isDeleted;
}
