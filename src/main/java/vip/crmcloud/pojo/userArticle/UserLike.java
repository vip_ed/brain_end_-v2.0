package vip.crmcloud.pojo.userArticle;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import lombok.Data;

import java.util.Date;

@TableName(value = "user_like")
@Data
public class UserLike {
    @TableId
    private String ulid;

    private String uid;

    private String aid;

    private String scid;

    private Date likeTime = new Date();

    private Long likeCount;

    @Version
    private Integer version;

    private Integer isDeleted;

    public UserLike() {
        this.likeCount = 0L; // 设置默认值为0
    }
}
