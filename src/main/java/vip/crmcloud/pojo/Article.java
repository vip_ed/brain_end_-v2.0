package vip.crmcloud.pojo;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * @TableName brain_article
 */
@TableName(value = "brain_article")
@Data
public class Article implements Serializable {
    private String scid;
    @TableId
    private String aid;

    private String uid;

    private String cid;

    private String articleTitle;

    private String articleContent;

    private Date articlePublishTime = new Date();

    private Date articleUpdateTime = new Date();

    private Integer articleViews;

    private Integer articleLike;

    private Integer articleStar;

    private String articleCoverimg;

//    private String userAvatar;
//    private String userNick;
    @Version
    private Integer version;

    private Integer isDeleted;

    // 单序列
    private static final long serialVersionUID = 1L;
}
