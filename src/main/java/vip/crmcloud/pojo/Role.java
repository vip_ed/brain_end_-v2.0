package vip.crmcloud.pojo;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import lombok.Data;

/**
 * @TableName brain_role
 */
@TableName(value ="brain_role")
@Data
public class Role implements Serializable {
    @TableId
    private String rid;

    private String roleName;

    @Version
    private Integer version;

    private Integer isDeleted;

    private static final long serialVersionUID = 1L;
}