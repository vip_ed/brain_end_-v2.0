package vip.crmcloud.pojo.article;

import com.baomidou.mybatisplus.annotation.Version;
import lombok.Data;

import java.util.Date;
@Data
public class ArticleDetail {
    private String scid;
    private String aid;

    private String uid;

    private String cid;

    private String articleTitle;

    private String articleContent;

    private Date articlePublishTime;

    private Date articleUpdateTime;

    private Integer articleViews;

    private Integer articleLike;

    private Integer articleStar;

    private String articleCoverimg;

    private String userAvatar;
    private String userNick;
    @Version
    private Integer version;

    private Integer isDeleted;
    // 详情需要的字段
    private String categoryName;
}
