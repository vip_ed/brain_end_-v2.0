package vip.crmcloud.pojo;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import lombok.Data;

/**
 * @TableName brain_study
 */
@TableName(value ="brain_study")
@Data
public class Study implements Serializable {
    @TableId
    private String sid;

    private String uid;

    private Integer studyFront;

    private Integer studyEnd;

    private Integer studyDatabase;

    private Integer studySafety;

    private Integer studyAi;

    @Version
    private Integer version;

    private Integer isDeleted;

    private static final long serialVersionUID = 1L;
}